using UnityEngine;
using System.Collections;

public class CharacterHandler : MonoBehaviour {
	
	public float MoveSpeed;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 temp = transform.rigidbody.velocity;
		temp.z = MoveSpeed * Input.GetAxis("Vertical");
		temp.x = MoveSpeed * Input.GetAxis("Horizontal");
		transform.rigidbody.velocity = temp;
		
		if(transform.position.y <= -20){
			GameObject bed = GameObject.Find("Bed") as GameObject;
			GameObject bedC = GameObject.Find("Bed(Clone)") as GameObject;
			
			if(bed){
			    transform.position = bed.transform.position;
			}
			if(bedC){
			    transform.position = bedC.transform.position;
			}
		}
	}
}
