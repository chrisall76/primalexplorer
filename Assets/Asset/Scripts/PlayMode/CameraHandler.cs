using UnityEngine;
using System.Collections;

public class CameraHandler : MonoBehaviour {
	
	public GameObject CharPrefab;
	public LevelSaving levelsave;
	
	//GUI		
    Vector3 scale;
    float originalWidth = 1280.0f;
    float originalHeight = 720.0f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
    	GameObject CharReg = GameObject.Find("Character") as GameObject;
		GameObject CharClone = GameObject.Find("Character(Clone)") as GameObject;
		if(CharReg){
            transform.parent = CharReg.transform;
		}
		if(CharClone){
		    transform.parent = CharClone.transform;
		}
		
		transform.localPosition = new Vector3(0, 15, -15);
	}
	
	void OnGUI(){
        scale.x = Screen.width / originalWidth;
        scale.y = Screen.height / originalHeight;
        scale.z = 1.0f;
        var svMat = GUI.matrix;
        GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);
		
		GUILayout.BeginHorizontal ("box");
		if(GUILayout.Button ("Load Level")){
			levelsave.LoadFromXml();
		}
		GUILayout.EndHorizontal ();
		GUI.matrix = svMat; // restore matrix
	}
}
