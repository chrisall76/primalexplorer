using UnityEngine;
using System.Collections;

public class MainMenuGUI : MonoBehaviour {
	
	public Texture tex;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI(){
		GUILayout.BeginArea (new Rect (15,10,100,100));
		GUILayout.Label(tex);
		GUILayout.EndArea();
		
		GUILayout.BeginArea (new Rect (15,95,100,100));
		if(GUILayout.Button ("Play")){
			Application.LoadLevel("PlayMode");
		}
		GUILayout.Space(15);
		if(GUILayout.Button ("Create")){
	        Application.LoadLevel("LevelCreate");
		}
	    GUILayout.EndArea();
	}
}
