using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelCreator : MonoBehaviour {
	
	public bool character = false;
	public bool raycastEnable = true;
	private int YRotation = 0;
	public AudioClip PlaceObj;
	public float MoveFCamZ = 15;
	public float PositionY = 0;
	
	public GameObject obj;
	public LevelSaving levelsave;
	public GameObject[] List;     //The objects that you will be able to place in-game
	public GameObject[] SpecialObj; //Put speical objects here (Player, spawn point, etc.)
	public List<GameObject> LevelObjects = new List<GameObject>();  //A list of all the objects in the scene
	
	private Vector2 mousePosInGUICoords;
	
	//GUI	
    Vector3 scale;
    float originalWidth = 1280.0f;
    float originalHeight = 720.0f;

	// Use this for initialization
	void Start () {
		obj = List[0];
	}
	
	// Update is called once per frame
	void Update () {
		if(Application.loadedLevel != 2){
		if(raycastEnable){
		if(Input.GetButtonDown ("Fire1")|| Input.GetButtonDown ("Fire1")){
				GameObject created;
			    Vector3 mousePos = Input.mousePosition;
			    mousePos.z = MoveFCamZ;
			
			    Vector3 objectPos = Camera.main.ScreenToWorldPoint(mousePos);
			    created = Instantiate(obj, objectPos, Quaternion.identity) as GameObject;
				if(obj != SpecialObj[0]){
			    Vector3 temp = created.transform.position;
			    temp.y = PositionY;
			    temp.x = Mathf.Round(temp.x);
			    temp.z = Mathf.Round(temp.z);
			    created.transform.position = temp;
                created.transform.rotation = Quaternion.Euler(0,YRotation,0);
				if(Application.loadedLevel == 1){
			        audio.PlayOneShot(PlaceObj);
				}
				}
				if(obj == SpecialObj[0]){
				    Vector3 temp = created.transform.position;
			        temp.y = PositionY + 0.5f;
			        temp.x = Mathf.Round(temp.x);
			        temp.z = Mathf.Round(temp.z);
			        created.transform.position = temp;
					created.transform.rotation = Quaternion.Euler(-90, 0, 0);
				}
			    LevelObjects.Add(created);
		    }
		}
		if(Input.GetButtonDown ("Fire2")){
		    RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			
			if(raycastEnable == true){
			    if (Physics.Raycast (ray, out hit, 100.0f)){
					if(hit.collider.name == "Character" || hit.collider.name == "Character(Clone)"){
						Debug.Log("hitCharacter");
						character = false;
					}
				    Destroy(hit.collider.gameObject);
				    LevelObjects.Remove(hit.collider.gameObject);
			    }
		    }
		}
		
		if(Input.GetKey(KeyCode.Alpha1)){
			obj = List[0];
		}
		if(Input.GetKey(KeyCode.Alpha2)){
			obj = List[1];
		}
		if(Input.GetKey(KeyCode.Alpha3)){
			obj = List[2];
		}
		if(Input.GetKey(KeyCode.Alpha4)){
			obj = List[3];
		}
		if(Input.GetKey(KeyCode.Alpha5)){
			obj = List[4];
		}
		if(Input.GetKey(KeyCode.Alpha6)){
			obj = SpecialObj[0];
		}
		if(Input.GetKey(KeyCode.Alpha7)){
			obj = SpecialObj[1];
		}
		
		if(Input.GetKeyDown(KeyCode.KeypadPlus)){
			YRotation += 90;
		}
		if(Input.GetKeyDown(KeyCode.KeypadMinus)){
			YRotation -= 90;
		}
		
		if(YRotation >= 360){
		    YRotation = 0;
		}
		if(YRotation <= -360){
		    YRotation = 0;
		}
	}
	}
	
	void OnGUI(){
        scale.x = Screen.width / originalWidth;
        scale.y = Screen.height / originalHeight;
        scale.z = 1.0f;
        var svMat = GUI.matrix;
        GUI.matrix = Matrix4x4.TRS(new Vector3(0,0,0), Quaternion.identity, scale);
		
		if(Application.loadedLevel == 1){
		mousePosInGUICoords = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		Rect rect1 = new Rect(0, 0, 91, 101);
		
		GUILayout.BeginArea (new Rect (0, 0, 90, 100));
		if(GUILayout.Button ("MainMenu")){
			PlayerPrefs.SetInt("LBCount",LevelObjects.Count);
			Application.LoadLevel("MainMenu");
		}
		if(gameObject.audio.mute == false){
		    if(GUILayout.Button ("MusicOff")){
			    gameObject.audio.mute = true;
		    }
		}
		if(gameObject.audio.mute == true){
		    if(GUILayout.Button ("MusicOn")){
			    gameObject.audio.mute = false;
		    }
		}
		if(GUILayout.Button ("Save Level")){
			levelsave.WriteToXmlFile(LevelObjects);
		}
		if(GUILayout.Button ("Load Level")){
			if(LevelObjects.Count > 0){
			    for(int n = 0; n < LevelObjects.Count; n++){
				    Destroy(LevelObjects[n]);
					LevelObjects.Remove(LevelObjects[n]);
			    }
			}
			levelsave.LoadFromXml();
		}
		
		if(rect1.Contains(mousePosInGUICoords)){
			raycastEnable = false;
		}else{
			raycastEnable = true;
		}
	    GUILayout.EndArea();
		
		GUI.Label (new Rect (5, 45, 100, 20), obj.name);
	    }
		
		GUI.matrix = svMat; // restore matrix
	}
	

}
